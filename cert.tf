provider "aws" {
  region = "${var.virginia}"
  alias  = "virginia"
}

resource "aws_acm_certificate" "dhcp-sucks" {
  provider          = "aws.virginia"
  domain_name       = "${var.root_domain}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "dhcp-cert-validation" {
  provider                = "aws.virginia"
  certificate_arn         = "${aws_acm_certificate.dhcp-sucks.arn}"
  validation_record_fqdns = ["${aws_route53_record.dhcp-cert-record.fqdn}"]
}

resource "aws_route53_record" "dhcp-cert-record" {
  provider = "aws.virginia"
  name     = "${aws_acm_certificate.dhcp-sucks.domain_validation_options.0.resource_record_name}"
  type     = "${aws_acm_certificate.dhcp-sucks.domain_validation_options.0.resource_record_type}"
  zone_id  = "${aws_route53_zone.dhcp-sucks.id}"
  records  = ["${aws_acm_certificate.dhcp-sucks.domain_validation_options.0.resource_record_value}"]
  ttl      = 60
}
