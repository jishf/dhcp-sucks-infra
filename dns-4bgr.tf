resource "aws_route53_zone" "4bgr" {
  name = "4bgr.scot"
}

resource "aws_route53_record" "gmail" {
  zone_id = "${aws_route53_zone.4bgr.zone_id}"
  name    = ""
  type    = "MX"

  records = [
    "1 ASPMX.L.GOOGLE.COM.",
    "5 ALT1.ASPMX.L.GOOGLE.COM.",
    "5 ALT2.ASPMX.L.GOOGLE.COM.",
    "10 ALT3.ASPMX.L.GOOGLE.COM.",
    "10 ALT4.ASPMX.L.GOOGLE.COM.",
    "15 xx5fbztiujaqfx4bxn56ap7kuvw3xok64nssw3fqdui3xpdzvliq.mx-verification.google.com.",
  ]

  ttl = "${var.mx_ttl}"
}
