resource "aws_route53_zone" "dhcp-sucks" {
  name = "${var.root_domain}"
}

/*resource "aws_route53_record" "gandi_mail" {
    zone_id = "${aws_route53_zone.dhcp-sucks.zone_id}"
    name = ""
    type = "MX"

    records = [
        "10 spool.mail.gandi.net.",
        "50 fb.mail.gandi.net."
    ]

    ttl = "${var.mx_ttl}"
}
*/

resource "aws_route53_record" "protonmail" {
  zone_id = "${aws_route53_zone.dhcp-sucks.zone_id}"
  name    = ""
  type    = "MX"

  records = [
    "10 mail.protonmail.ch.",
    "20 mailsec.protonmail.ch.",
  ]

  ttl = "${var.mx_ttl}"
}

resource "aws_route53_record" "txt_records" {
  zone_id = "${aws_route53_zone.dhcp-sucks.zone_id}"
  name    = ""
  type    = "TXT"

  records = ["protonmail-verification=86b01499fc72e8ac7cea29a7537dcebc0ee78924",
    "v=spf1 include:_spf.protonmail.ch mx ~all",
    "google-site-verification=H_Acd402YwliUgQLo5PegRgx0utY90ZRS5Uoyz71lLQ",
  ]

  ttl = "${var.mx_ttl}"
}

/*
resource "aws_route53_record" "gandi_spf" {
    zone_id = "${aws_route53_zone.dhcp-sucks.zone_id}"
    name = ""
    type = "TXT"

    records = ["v=spf1 include:_mailcust.gandi.net ?all"]

    ttl = "${var.mx_ttl}"
}
*/

resource "aws_route53_record" "protonmail_dkim" {
  zone_id = "${aws_route53_zone.dhcp-sucks.zone_id}"
  name    = "protonmail._domainkey"
  type    = "TXT"

  records = ["v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC1/Ogkz9TKWUYgXZYaEHt/oi1o8JUlhn2gR/7UbTe+UlWwfeQWTgah0V65tMTgsgIXO4x9nYCGmxQIIt/UkHAWVfEVTZNCYG5uApQiy03qrv3Sriw3udj4ebUvdDOtUDKL1xVT5H2EIjoOueq0uf5axhOvxo48du1R4HgYlBtnRQIDAQAB"]

  ttl = "${var.mx_ttl}"
}

resource "aws_route53_record" "protonmail_dmarc" {
  zone_id = "${aws_route53_zone.dhcp-sucks.zone_id}"
  name    = "_dmarc"
  type    = "TXT"

  records = ["v=DMARC1; p=none; rua=mailto:security@dhcp.sucks"]

  ttl = "${var.mx_ttl}"
}

resource "aws_route53_record" "dhcp-sucks-a" {
  zone_id = "${aws_route53_zone.dhcp-sucks.zone_id}"
  name    = "${var.root_domain}"
  type    = "A"

  alias = {
    name                   = "${aws_cloudfront_distribution.dhcp-sucks-cdn.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.dhcp-sucks-cdn.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "dhcp-sucks-caa" {
  zone_id = "${aws_route53_zone.dhcp-sucks.zone_id}"
  name    = ""
  type    = "CAA"

  records = [
    "0 issue \"amazon.com\"",
    "0 issue \"amazontrust.com\"",
    "0 issue \"awstrust.com\"",
    "0 issue \"amazonaws.com\"",
  ]

  ttl = "${var.mx_ttl}"
}

output "name_servers" {
  value = "${aws_route53_zone.dhcp-sucks.name_servers}"
}
