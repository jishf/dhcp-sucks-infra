// S6 bucket

resource "aws_s3_bucket" "dhcp-sucks-ionfs" {
  bucket = "dhcp-sucks-ionfs"
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_iam_policy" "s3-ionfs" {
  name        = "s3-ionfs"
  path        = "/"
  description = "s3 policy for ionfs"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "S3Ionfs",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "${aws_s3_bucket.dhcp-sucks-ionfs.arn}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetBucket",
                "s3:ListBucket"
            ],
            "Resource": [
                "${aws_s3_bucket.dhcp-sucks-ionfs.arn}"
            ]
        }
    ]
}

EOF
}

// NKV bucket

resource "aws_s3_bucket" "dhcp-sucks-ionfs-nkv" {
  bucket = "dhcp-sucks-ionfs-secrets"
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_iam_policy" "s3-ionfs-nkv" {
  name        = "s3-ionfs-nkv"
  path        = "/"
  description = "s3 policy for ionfs nkv bucket"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "S3Ionfs",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "${aws_s3_bucket.dhcp-sucks-ionfs-nkv.arn}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetBucket",
                "s3:ListBucket"
            ],
            "Resource": [
                "${aws_s3_bucket.dhcp-sucks-ionfs-nkv.arn}"
            ]
        }
    ]
}

EOF
}
