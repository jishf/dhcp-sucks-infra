provider aws {
  region = "${var.london}"
}

resource "aws_s3_bucket" "dhcp-sucks" {
  bucket = "${var.root_domain}"
  acl    = "public-read"

  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"AddPerm",
      "Effect":"Allow",
      "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::${var.root_domain}/*"]
    }
  ]
}
POLICY

  website {
    index_document = "index.html"
    error_document = "404.html"
  }
}

output "dhcp-sucks" {
  value = ["${aws_s3_bucket.dhcp-sucks.*.bucket_domain_name}"]
}

terraform {
  backend "s3" {}
}
