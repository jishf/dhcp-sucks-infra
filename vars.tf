#variable "aws_access_key" {}
#variable "aws_secret_key" {}

variable "dublin" {
  default = "eu-west-1"
}

variable "london" {
  default = "eu-west-2"
}

variable "virginia" {
    default = "us-east-1"
}

variable "root_domain" {
  default = "dhcp.sucks"
}
variable "www_domain" {
  default = "www.dhcp.sucks"
}

variable "mx_ttl" {
    default = "10800"
}